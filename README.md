Prudential ICN installation with respect to the type of repository(CM or FileNet)

Ansible playbook to Install ICN with CM repo type.

Variables that can be passed dynamically during playbook execution: 
    
    run_as - User to run the playbook
    icn_repo_type - Specify CM or FileNet configuration(mention either CM or FileNet) 
    cm_svr_hostkey - specify the required hostkey for CM configuration

Values of variables that are unknown and need be defined in defaults of icn-install role: 

    icn_Was_Federated_BaseEntry_DNRepository 
    icn_JDBC_DataSource_Name
    icn_filenetP8_Object_Store 
    icn_connection_Community_URL
    icn_connections_Community_HTTPS_URL

Running Playbook Example
    
    ansible-playbook -i inventories -e "host_group=icn-cm-uat" icn-playbook.yml

Inventory Update hostnames in inventories/hosts file. Example:

    [icn-cm-uat] 10.0.0.1

Roles: Below is the role used in this playbook

    icn-install -- Installs ICN with configurations respect to CM or Filenet

WAS Supported Version: 9, Ansible Supported Version: 2.4.2.0

    [+] run_as                                   # User to run the playbook
    [+] become_user_var                          # Operating System User Account for root
    [+] become_method_var                        # Operating System Command To escalate priviledge to root
    [+] icn_profile_name                         # ICN Profile name, default: AppSvrICN01
    [+] icn_server_name                          # ICN Server name, default: afcicnsrv
    [+] icn_admin_username                       # ICN admin user
    [+] icn_admin_group                          # ICN admin group
    [+] icn_admin_password                       # ICN admin password
    [+] filenet_svr_hostkey                      # FILENET server hostkey under hosts file, ex: FILENET1
    [+] was_profile_name                         # Filenet Profile name, default: AppSvrFN01
    [+] was_server_name                          # Filenet Server name, default: afcfnsrv
    [+] was_admin_username                       # Filenet admin user
    [+] was_admin_group                          # Filenet admin group
    [+] was_admin_password                       # Filenet admin password
    [+] was_license_caption                      # Filenet License content, default: PVU Non-Production
    [+] database_servername                      # Filenet database servername
    [+] database_name_gcd                        # Filenet database name for gcd, default: GCDDB
    [+] database_name_os                         # Filenet database name for os, default: OSDB
    [+] database_portnumber                      # Filenet database portnumber, default: 55701
    [+] database_username                        # Filenet database user
    [+] database_password                        # Filenet database password
    [+] was_ldap_server_host                     # Filenet Ldap Server Host
    [+] was_ldap_server_port                     # Filenet Ldap Server Port, default: 636
    [+] was_ldap_server_bind_dn                  # Filenet Ldap Server Bind DN
    [+] was_ldap_server_base_dn                  # Filenet Ldap Server Base DN
    [+] was_ldap_admin_console_user              # Filenet Ldap Server Admin Console User
    [+] was_soap_login_userid                    # Filenet soap login userid, default: SRVAFRHOUECMADM
    [+] was_soap_login_password                  # Filenet soap login passwd, default: <redacted>
    [+] icn_ldap_server_host                     # ICN Ldap Server Host
    [+] icn_ldap_server_port                     # ICN Ldap Server Port, default: 389
    [+] icn_ldap_server_bind_dn                  # ICN Ldap Server Bind DN
    [+] icn_ldap_server_base_dn                  # ICN Ldap Server Base DN
    [+] icn_ldap_admin_console_user              # ICN Ldap Server Admin Console User
    [+] icn_ltpa_key_path                        # ICN LTPA Key Path, default: /opt/IBM/ECMClient/FN_LTPA/FN_LTPA_KEY
    [+] icn_ltpa_key_password                    # ICN LTPA Key Password
    [+] icn_cepeusername                         # ICN Cepe Username
    [+] icn_database_schema                      # ICN Database Schema
    [+] icn_database_servername                  # ICN Database Server Name
    [+] icn_database_portnumber                  # ICN Database Port Number, default: 55701
    [+] icn_database_name                        # ICN Database Name, default: ICNDB
    [+] icn_database_username                    # ICN Database username
    [+] icn_database_password                    # ICN Database password
    [+] icn_tablespace_name                      # ICN Tablespace Name, default: ICNTS
    [+] icn_soap_login_userid                    # ICN Soap login userid, default: SRVAFRHOUECMADM
    [+] icn_soap_login_password                  # ICN Soap login passwd, default: <redacted>
    [+] cmis_ldap_server_host                    # CMIS Ldap Server Host
    [+] cmis_ldap_server_port                    # CMIS Ldap Server Port, default: 389
    [+] cmis_ldap_server_bind_dn                 # CMIS Ldap Server Bind DN
    [+] cmis_ldap_server_base_dn                 # CMIS Ldap Server Base DN
    [+] cmis_ldap_admin_console_user             # CMIS Ldap Server Admin Console User
    [+] cmis_ltpa_key_path                       # CMIS LTPA Key Path, default: /opt/IBM/CMIS/FN_LTPA/FN_LTPA_KEY
    [+] cmis_ltpa_key_password                   # CMIS LTPA Key Password
	[+] fn_http_postinst_password                # Http Server postinst Password
	[+] fn_http_server_admin_port                # Http Server admin Port
	[+] fn_http_server_admin_user                # http server admin user
	[+] fn_http_server_admin_grp                 # Http Server admin grp
	[+] fn_http_htadmin_id                       # Http htadmin id
	[+] fn_http_htadmin_password                 # Http htadmin password  
    [+] cyberark_ldapbindpassword                # Cyber ARK Ldap Bind Password
    [+] cyberark_cepeuserpassword                # Cyber ARK Cepe User Password
# updated variables for ICN 
    [+]icn_repo_type				 			 # ICN repository type 
    [+]icn_Was_Federated_BaseEntry_DNRepository  # ICN Was Federated BaseEntry DNRepository
    [+]cm_svr_hostkey                            # CM Server hostkey
    [+]cm_svr_hostname                           # CM Server hostname
    [+]icn_JDBC_DataSource_Name                  # ICN JDBC DataSource Name
	[+]icn_connection_Community_URL              # ICN connection Community URL
    [+]icn_connections_Community_HTTPS_URL	     # ICN connection Community HTTPS URL

Filenet Config Looping Variables:

    [+] fn_configurer_structure_modifier:
          - was_profile_name: 'AppSvrFN01'
            was_server_name: 'afcfnsrv'
            was_config_profile_name: 'fnprofile'
            was_host_name: '{{was_host_name}}'
            was_admin_username: '{{was_admin_username}}'
            was_admin_password: '{{was_admin_password}}'
            was_server_node: "{{was_server_node}}"
            was_ldap_server_host: "{{was_ldap_server_host}}"
            was_ldap_server_port: "{{was_ldap_server_port}}"
            was_ldap_server_bind_dn: "{{was_ldap_server_bind_dn}}"
            cyberark_ldapbindpassword: "{{cyberark_ldapbindpassword}}"
            was_ldap_server_base_dn: "{{was_ldap_server_base_dn}}"
            was_ldap_admin_console_user: "{{was_ldap_admin_console_user}}"
            database_servername: "{{database_servername}}"
            database_portnumber: "{{database_portnumber}}"
            database_username: "{{database_username}}"
            database_password: "{{database_password}}"
            database_name_os: "{{database_name_os}}"
            database_name_gcd: "{{database_name_gcd}}"
            was_soap_login_userid: "{{was_soap_login_userid}}"
            was_soap_login_password: "{{was_soap_login_password}}"
            was_jndi_name: "{{was_jndi_name}}"
            was_jndi_xaname: "{{was_jndi_xaname}}"
            was_server_cell: "{{was_server_cell}}"
            was_server_xml_before_verbosemodejni_attributes: '{{was_server_xml_before_verbosemodejni_attributes}}'
            was_server_xml_before_runhprof_attributes: '{{was_server_xml_before_runhprof_attributes}}'

ICN Config Looping Variables:

    [+] icn_configurer_structure_modifier:
          - icn_profile_name: 'AppSvrICN01'
            icn_server_name: 'afcicnsrv'
            icn_config_profile_name: 'saicnprf'
            icn_host_name: '{{icn_host_name}}'
            icn_admin_username: '{{icn_admin_username}}'
            icn_admin_password: '{{icn_admin_password}}'
            icn_server_node: '{{icn_server_node}}'
            icn_ldap_server_host: '{{icn_ldap_server_host}}'
            icn_ldap_server_port: '{{icn_ldap_server_port}}'
            icn_ldap_server_bind_dn: '{{icn_ldap_server_bind_dn}}'
            cyberark_ldapbindpassword: "{{cyberark_ldapbindpassword}}"
            icn_ldap_server_base_dn: '{{icn_ldap_server_base_dn}}'
            icn_ldap_admin_console_user: '{{icn_ldap_admin_console_user}}'
            icn_ltpa_key_path: '{{icn_ltpa_key_path}}'
            icn_ltpa_key_password: '{{icn_ltpa_key_password}}'
            icn_cepeusername: '{{icn_cepeusername}}'
            cyberark_cepeuserpassword: '{{cyberark_cepeuserpassword}}'
            icn_database_schema: '{{icn_database_schema}}'
            icn_database_servername: '{{icn_database_servername}}'
            icn_database_portnumber: '{{icn_database_portnumber}}'
            icn_database_name: '{{icn_database_name}}'
            icn_database_username: '{{icn_database_username}}'
            icn_database_password: '{{icn_database_password}}'
            icn_ecmclient_admin_name: '{{icn_ecmclient_admin_name}}'
            icn_tablespace_name: '{{icn_tablespace_name}}'
            icn_jdbc_dir: '{{icn_jdbc_dir}}'
            icn_rundbfill: '{{icn_rundbfill}}'
            icn_dbfailover_enabled: '{{icn_dbfailover_enabled}}'
            icn_soap_login_userid: '{{icn_soap_login_userid}}'
            icn_soap_login_password: '{{icn_soap_login_password}}'
            icn_server_cell: '{{icn_server_cell}}'
            icn_ce_port_number: "{{icn_ce_port_number}}"
            icn_server_xml_before_verbosemodejni_attributes: '{{icn_server_xml_before_verbosemodejni_attributes}}'
            icn_server_xml_before_runhprof_attributes: '{{icn_server_xml_before_runhprof_attributes}}'

ICN Config Looping Variables:

    [+] cmis_configurer_structure_modifier:
          - cmis_config_profile_name: 'saicncmisprf'
            cmis_server_name: '{{cmis_server_name}}'
            cmis_profile_name: '{{cmis_profile_name}}'
            cmis_admin_username: '{{cmis_admin_username}}'
            cmis_admin_password: '{{cmis_admin_password}}'
            cmis_server_cell: '{{cmis_server_cell}}'
            cmis_host_name: '{{cmis_host_name}}'
            cmis_server_node: '{{cmis_server_node}}'
            cmis_ldap_server_host: '{{cmis_ldap_server_host}}'
            cmis_ldap_server_port: '{{cmis_ldap_server_port}}'
            cmis_ldap_server_bind_dn: '{{cmis_ldap_server_bind_dn}}'
            cyberark_ldapbindpassword: "{{cyberark_ldapbindpassword}}"
            cmis_ldap_server_base_dn: '{{cmis_ldap_server_base_dn}}'
            cmis_ldap_admin_console_user: '{{cmis_ldap_admin_console_user}}'
            cmis_ltpa_key_path: '{{cmis_ltpa_key_path}}'
            cmis_ltpa_key_password: '{{cmis_ltpa_key_password}}'
            cmis_bootstrap_port: '{{cmis_bootstrap_port}}'
			
			
HTTP Config Looping Variables:

    [+] http_configurer_structure_modifier: 
          - http_webserver_name: 'webserverZM'
            http_profile_name: 'AppSvrZMICN01'
            http_server_name: 'afczmicnsrv'
            http_admin_username: '{{http_admin_username}}'
            http_admin_password: '{{http_admin_password}}'
            http_kdb_filename: 'keyZM.kdb'
            http_cert_label: 'icn-uat-zm'
            http_csr_filename: 'cert.csr'
            http_dn_cert: '{{http_dn_cert}}'
            httpd_conf_vh_default_name: '{{httpd_conf_vh_default_name}}'
            httpd_conf_host_name: '{{httpd_conf_host_name}}'
            http_httpdconf_name: 'httpdZM.conf'
            http_keyring_processname: '{{http_keyring_processname}}'
            http_keyring_node: '{{http_keyring_node}}'
            http_keyring_cell: '{{http_keyring_cell}}'
            http_htadmin_name: '{{http_htadmin_name}}'
            http_htadmin_password: '{{http_htadmin_password}}'
            http_htadmin_port: '{{http_htadmin_port}}'
            http_htadmin_webport: '{{http_htadmin_webport}}'
            http_sslcert_name: '{{http_sslcert_name}}'
            http_pidfile_name: '{{http_pidfile_name}}'
            http_accesslog_file: '{{http_accesslog_file}}'
            http_errorlog_file: '{{http_errorlog_file}}'
            http_listener_ip: '{{http_listener_ip}}'

